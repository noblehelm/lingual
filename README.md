# Lingual

Lingual is a tool to create your own language learning environment. It currently is under development.

## Features

*Since Lingual is under development, some of the features here are subjected to change.*

Lingual is (supposed to be) a simple tool to create a language learning environment. It should allow the creation of language lessons, with each lesson featuring any number of tasks. These tasks have two simple components, the question and the answer, and additional components, such as the type of question (e.g.: drag-and-drop, multiple choice, fill-in-the-blanks) and a media (e.g.: images, audio, video) associated with it.

The language environment should also feature grammar tables and a vocabulary. The grammar tables are instructions on how to construct phrases or text based on the grammar rules of the language.

Finally, the tool should support different origin languages. For instance, instead of supporting learning Chinese only from an English perspective, it should also support learning from an Italian or French perspective.
