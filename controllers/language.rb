require 'pg'

def get_languages(connection)
    result = connection.exec 'SELECT language_id,name FROM languages;'
    language_array = []
    result.each do |rs|
        language_array << {
            'language_id': rs['language_id'].to_i,
            'name': rs['name']
        }
    end
    return language_array.to_json
rescue PG::Error => e
    puts e.message
end

# Returns information of a single language
def get_language(connection, lang_id)
    result = connection.exec_params 'SELECT language_id, name FROM languages WHERE language_id=$1;', [lang_id.to_i]
    return JSON.generate({
        'language_id': result[0]['language_id'].to_i,
        'name': result[0]['name'],
        'lessons': "/languages/#{lang_id}/lessons"
    })
rescue PG::Error => e
    puts e.message
end

def post_languages(connection, json_data)
    connection.transaction do |ct|
        json_data.each do |jd|
            ct.exec_params 'INSERT INTO languages(name) VALUES ($1)', [jd['language_name']]
        end
    end
rescue PG::Error => e
    puts e.message
    return -1
end

def delete_language(connection, lang_id)
    connection.exec_params 'DELETE FROM languages WHERE language_ud=$1;', [lang_id.to_i]
rescue PG::Error => e
    puts e.message
    return -1
end

def delete_languages(connection)
    connection.exec 'DELETE FROM languages;'
rescue PG::Error => e
    puts e.message
    return -1
end
