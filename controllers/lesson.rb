require 'pg'

# Returns a list of lessons that belong to a language
def get_lessons(connection, lang_id)
    result = connection.exec_params 'SELECT lesson_id,name FROM lessons WHERE language_id=$1;', [lang_id]
    lesson_array = []
    result.each do |rs|
        lesson_array << {
            'lesson_id': rs['lesson_id'].to_i,
            'name': rs['name']
        }
    end
    return lesson_array.to_json
rescue PG::Error => e
    puts e.message
end

# Returns information on a single lesson
def get_lesson(connection, language_id, lesson_id)
    result = connection.exec_params 'SELECT name FROM lessons WHERE lesson_id=$1;', [lesson_id]
    result_lesson = nil
    result.each do |rs|
        result_lesson = {
            'lesson_id': lesson_id.to_i,
            'name': rs['name'],
            'tasks': "/languages/#{language_id}/lessons/#{lesson_id}/tasks"
        }
    end
    return result_lesson.to_json
rescue PG::Error => e
    puts e.message
end

# Creates a new lesson in the specified language id
def post_lessons(connection, lang_id, json_data)
    connection.transaction do |ct|
        json_data.each do |jd|
            ct.exec_params 'INSERT INTO lessons (language_id, name) VALUES ($1, $2);', [lang_id, jd['lesson_name']]
        end
    end
rescue PG::Error => e
    puts e.message
    return -1
end

# Deletes a single lesson specified by a lesson id
def delete_lesson(connection, less_id)
    connection.exec_params 'DELETE FROM lessons WHERE lesson_id=$1;', [less_id]
rescue PG::Error => e
    puts e.message
    return -1
end

# Deletes lessons via the DELETE http method in the main lesson collection endpoint
def delete_lessons(connection, language_id)
    connection.exec_params 'DELETE FROM lessons WHERE language_id=$1;', [language_id.to_i]
rescue PG::Error => e
    puts e.message
    return -1
end
