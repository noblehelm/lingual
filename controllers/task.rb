require 'pg'

# Returns a list of tasks that belongs to a lesson
def get_tasks(connection, less_id)
    result = connection.exec_params "SELECT task_id,question FROM tasks WHERE lesson_id=$1;", [less_id.to_i]
    task_array = []
    result.each do |rs|
        task_array << {
            "task_id": rs["task_id"].to_i,
            "question": rs["question"]
        }
    end
    return task_array.to_json
rescue PG::Error => e
    puts e.message
end

# Returns the contents of a single task
def get_task(connection, task_id)
    result = connection.exec_params "SELECT question, answer FROM tasks WHERE task_id=$1;", [task_id.to_i]
    result_task = []
    result.each do |rs|
        result_task = {
            "task_id": task_id.to_i,
            "question": rs["question"],
            "answer": rs["answer"]
        }
    end
    return result_task.to_json
rescue PG::Error => e
    puts e.message
end

# Creates a new task in the specified lesson
def post_tasks(connection, less_id, json_data)
    connection.transaction do |ct|
        json_data.each do |jd|
            ct.exec_params "INSERT INTO tasks (question, answer, lesson_id) VALUES ($1, $2, $3);", [jd["question"], jd["answer"], less_id.to_i]
        end
    end
rescue PG::Error => e
    puts e.message
    return -1
end

# Deletes a collection of tasks of a specified lesson
def delete_tasks(connection, lesson_id)
    connection.exec_params "DELETE FROM tasks WHERE lesson_id=$1;", [lesson_id.to_i]
rescue PG::Error => e
    puts e.message
    return -1
end

# Deletes a single task
def delete_task(connection, task_id)
    connection.exec_params "DELETE FROM tasks WHERE task_id=$1;", [task_id.to_i]
rescue PG::Error => e
    puts e.message
    return -1
end
