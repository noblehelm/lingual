require 'sinatra'
require_relative 'controllers/language'
require_relative 'controllers/lesson'
require_relative 'controllers/task'
require 'json'

before do
    @con = PG.connect :host => 'localhost', :dbname => 'lingual', :user => 'postgres', :password => ''
    response.headers["Access-Control-Allow-Origin"] = "*"
end

# list all languages available
get '/languages' do
    content_type :json
    get_languages(@con)
end

# get information on a language
get '/languages/:language_id' do
    content_type :json
    get_language(@con, params["language_id"])
end

# list a language's lessons
get '/languages/:language_id/lessons' do
    content_type :json
    get_lessons(@con, params["language_id"])
end

# get information on a lesson
get '/languages/:language_id/lessons/:lesson_id' do
    content_type :json
    get_lesson(@con, params["language_id"],params["lesson_id"])
end

# list a lesson's tasks
get '/languages/:language_id/lessons/:lesson_id/tasks' do
    content_type :json
    get_tasks(@con, params["lesson_id"])
end

# get a task information (e.g.: question, answer, media)
get '/languages/:language_id/lessons/:lesson_id/tasks/:task_id' do
    content_type :json
    get_task(@con, params["task_id"])
end

#
# From now on, all requests must have authentication and authorization.
#

# Creates one or more new languages
post '/languages' do
    data = JSON.parse(request.body.read)
    if post_languages(@con, data) != -1
        status 201
    else
        status 500
    end
end

# Creates one or more new lessons in a language
post '/languages/:language_id/lessons' do
    data = JSON.parse(request.body.read)
    if post_lessons(@con, params["language_id"],data) != -1
        status 201
    else
        status 500
    end
end

post '/languages/:language_id/lessons/:lesson_id/tasks' do
    data = JSON.parse(request.body.read)
    if post_tasks(@con, params["lesson_id"], data) != -1
        status 201
    else
        status 500
    end
end

delete '/languages' do
    if delete_languages(@con) != -1
        status 200
    else
        status 500
    end
end

delete '/languages/:language_id' do
    if delete_language(@con, params["language_id"]) != -1
        status 200
    else
        status 500
    end
end

delete '/languages/:language_id/lessons' do
    if delete_lessons(@con, params['language_id']) != -1
        status 200
    else
        status 500
    end
end

delete '/languages/:language_id/lessons/:lesson_id' do
    if delete_lesson(@con, params["lesson_id"]) != -1
        status 200
    else
        status 500
    end
end

delete '/languages/:language_id/lessons/:lesson_id/tasks' do
    if delete_tasks(@con, params["lesson_id"]) != -1
        status 200
    else
        status 500
    end
end

delete '/languages/:language_id/lessons/:lesson_id/tasks/:task_id' do
    if delete_task(@con, params["task_id"]) != -1
        status 200
    else
        status 500
    end
end


after do
    @con.close if @con
end
