require 'pg'
require_relative 'database/connection'


def tables_setup(connection)
    begin
        con = PG.connect :host => connection.dbhost, :dbname => connection.dbname, :user => connection.dbuser, :password => connection.dbpass
        con.transaction do |ct|
            ct.exec "CREATE TABLE IF NOT EXISTS languages(language_id serial PRIMARY KEY, name VARCHAR(50) UNIQUE NOT NULL);"
            ct.exec "CREATE TABLE IF NOT EXISTS lessons(lesson_id serial PRIMARY KEY, language_id int REFERENCES languages(language_id), name VARCHAR(50));"
            ct.exec "CREATE TABLE IF NOT EXISTS task_type(task_type_id serial PRIMARY KEY, name VARCHAR(50) UNIQUE NOT NULL);"
            ct.exec "CREATE TABLE IF NOT EXISTS tasks(task_id serial PRIMARY KEY, question text, answer text, lesson_id int REFERENCES lessons(lesson_id), task_type_id int REFERENCES task_type(task_type_id));"
            ct.exec "CREATE TABLE IF NOT EXISTS media_type(media_type_id serial PRIMARY KEY, name VARCHAR(50) UNIQUE NOT NULL);"
            ct.exec "CREATE TABLE IF NOT EXISTS media(media_id serial PRIMARY KEY, name VARCHAR(50), description VARCHAR(255), path VARCHAR(255) UNIQUE NOT NULL, media_type int REFERENCES media_type(media_type_id));"
            ct.exec "CREATE TABLE IF NOT EXISTS gtables(gtable_id serial PRIMARY KEY, name VARCHAR(127) NOT NULL, instructions text, language_id int REFERENCES languages(language_id));"
            ct.exec "CREATE TABLE IF NOT EXISTS task_has_media (task_id int REFERENCES tasks(task_id), media_id int REFERENCES media(media_id), PRIMARY KEY (task_id, media_id));"
            ct.exec "CREATE TABLE IF NOT EXISTS gtable_has_media (gtable_id int REFERENCES gtables(gtable_id), media_id int REFERENCES media(media_id), PRIMARY KEY (gtable_id, media_id));"
        end
    rescue PG::Error => e
        puts e.message
    ensure
        con.close if con
    end
end

def tables_teardown(connection)
    begin
        con = PG.connect :host => connection.dbhost, :dbname => connection.dbname, :user => connection.dbuser, :password => connection.dbpass
        con.transaction do |ct|
            ct.exec "DROP TABLE IF EXISTS media_type CASCADE;"
            ct.exec "DROP TABLE IF EXISTS media CASCADE;"
            ct.exec "DROP TABLE IF EXISTS task_has_media CASCADE;"
            ct.exec "DROP TABLE IF EXISTS gtable_has_media CASCADE;"
            ct.exec "DROP TABLE IF EXISTS gtables CASCADE;"
            ct.exec "DROP TABLE IF EXISTS tasks CASCADE;"
            ct.exec "DROP TABLE IF EXISTS lessons CASCADE;"
            ct.exec "DROP TABLE IF EXISTS languages CASCADE;"
            ct.exec "DROP TABLE IF EXISTS task_type CASCADE;"
        end
    rescue PG::Error => e
        puts e.message
    ensure
        con.close if con
    end
end


if ARGV[0] == 'setup'
    conexao = Connection.new('localhost', 'lingual', 'postgres', '')
    tables_setup(conexao)
elsif ARGV[0] == 'teardown'
    conexao = Connection.new('localhost', 'lingual', 'postgres', '')
    tables_teardown(conexao)
end
